package com.farm.llm.domain;

import java.util.ArrayList;
import java.util.List;

public class LlmRefMaterials {
	private List<ReferenceMaterial> list = new ArrayList<ReferenceMaterial>();

	public List<ReferenceMaterial> getList() {
		return list;
	}

	public void setList(List<ReferenceMaterial> list) {
		this.list = list;
	}

}
