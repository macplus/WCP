package com.farm.aliyun.test;

import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;

//Copyright (c) Alibaba, Inc. and its affiliates.

import com.alibaba.dashscope.aigc.conversation.Conversation;
import com.alibaba.dashscope.aigc.conversation.ConversationParam;
import com.alibaba.dashscope.aigc.conversation.ConversationResult;
import com.alibaba.dashscope.aigc.generation.Generation;
import com.alibaba.dashscope.aigc.generation.GenerationResult;
import com.alibaba.dashscope.aigc.generation.models.QwenParam;
import com.alibaba.dashscope.common.Message;
import com.alibaba.dashscope.common.ResultCallback;
import com.alibaba.dashscope.common.Role;
import com.alibaba.dashscope.exception.ApiException;
import com.alibaba.dashscope.exception.InputRequiredException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.alibaba.dashscope.utils.Constants;
import com.alibaba.dashscope.utils.JsonUtils;
import com.farm.llm.FarmLlmInter;
import com.farm.llm.aliyun.client.AliyunLlmClient;

import io.reactivex.Flowable;

public class AliLLMQuest {
	public static void main(String[] args) throws Exception {
		Constants.apiKey = "sk-2cf870a4457a466a837bfd319587248f";
		FarmLlmInter llm = AliyunLlmClient.getInstance(Constants.apiKey, "text-embedding-v1", "qwen-plus", 3500,13000);
		String question = "有没有关于wcp的默认登录密码的知识，地址是什么";
		String msg = llm.sendMsg("帮我我找到这句话中不超过3个最重要的关键字，作为全文检索的关键字，用《》括起来:<<" + question + ">>");
		// String msg = llm.sendMsg("我需要使用全文检索来找到这个问题的答案:<<" + question +
		// ">>，现在帮我提炼不超过3个最能找到答案的关键字，用《》括起来返回给我");
		System.out.println(msg);

		FarmLlmInter llm2 = AliyunLlmClient.getInstance(Constants.apiKey, "text-embedding-v1", "qwen-plus", 3500,13000);
		System.out.println(llm2.getEmbedding("哈哈哈").getEmbeddingFloats());

	}
}