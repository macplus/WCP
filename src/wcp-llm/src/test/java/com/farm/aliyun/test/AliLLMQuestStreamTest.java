package com.farm.aliyun.test;

import java.util.function.BiConsumer;

import com.farm.llm.FarmLlmInter;
import com.farm.llm.aliyun.client.AliyunLlmClient;
import com.farm.llm.domain.LlmMessages;

public class AliLLMQuestStreamTest {

	public static void main(String[] args) throws Exception {
		FarmLlmInter llm = AliyunLlmClient.getInstance("sk-2cf870a4457a466a837bfd319587248f", "text-embedding-v1",
				"qwen-plus", 3500,13000);
		llm.sendMsg(LlmMessages.getInstance("写首小诗"), new BiConsumer<String, Boolean>() {
			@Override
			public void accept(String t, Boolean u) {
				if (u) {
					System.out.println(t);
				} else {
					System.out.print(t);
				}
			}
		},1,500);
	}
}