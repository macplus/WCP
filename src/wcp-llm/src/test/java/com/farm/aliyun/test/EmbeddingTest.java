package com.farm.aliyun.test;

import com.alibaba.dashscope.exception.ApiException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.alibaba.dashscope.utils.Constants;
import com.farm.llm.FarmEmbeddings;

import java.util.Arrays;
import java.util.List;

import com.alibaba.dashscope.embeddings.TextEmbedding;
import com.alibaba.dashscope.embeddings.TextEmbeddingParam;
import com.alibaba.dashscope.embeddings.TextEmbeddingResult;

public final class EmbeddingTest {
	public static void basicCall() throws ApiException, NoApiKeyException {
		TextEmbeddingParam param = TextEmbeddingParam.builder().model(TextEmbedding.Models.TEXT_EMBEDDING_V1)
				.texts(Arrays.asList("风急天高1高11212312高11212312高11212312高11212312高11212312高1121231212123123猿啸哀, 渚清沙白鸟飞回, 无边落木萧萧下, 不尽长江滚滚来")).build();
		TextEmbedding textEmbedding = new TextEmbedding();
		TextEmbeddingResult result = textEmbedding.call(param);
		List<Double> embedding = result.getOutput().getEmbeddings().get(0).getEmbedding();

		byte[] ebyte = FarmEmbeddings.doubleArrayToByteArray(FarmEmbeddings.convertListToDoubleArray(embedding));
		System.out.println(embedding.size());
		double[] emb = FarmEmbeddings.byteArrayToDoubleArray(ebyte, embedding.size());

		System.out.println(emb);
	}

	public static void main(String[] args) {
		Constants.apiKey = "sk-2cf870a4457a466a837bfd319587248f";
		try {
			basicCall();
		} catch (ApiException | NoApiKeyException e) {
			System.out.println(e.getMessage());
		}
		System.exit(0);
	}
}
