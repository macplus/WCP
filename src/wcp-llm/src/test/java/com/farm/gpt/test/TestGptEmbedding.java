package com.farm.gpt.test;

import com.farm.llm.FarmLlmInter;
import com.farm.llm.openai.client.ChatGptClient;

/**
 * GPT接口封装
 * 
 * @author macpl
 *
 */
public class TestGptEmbedding {

	public static void main(String[] args) {
		FarmLlmInter client = ChatGptClient.getInstance("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",
				"text-embedding-3-small", "gpt-3.5-turbo", 3500, "127.0.0.1", 7890, "https://api.openai.com/");
		try {
			System.out.println(client.getEmbedding("测试").getEmbeddingFloats().length);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
