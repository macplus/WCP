package com.farm.gpt.test;

import java.util.function.BiConsumer;

import com.farm.llm.FarmLlmInter;
import com.farm.llm.domain.LlmMessages;
import com.farm.llm.openai.client.ChatGptClient;

public class TestGptStream {
	public static void main(String[] args) {
		FarmLlmInter client = ChatGptClient.getInstance("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", null,
				"gpt-3.5-turbo", 3500, "127.0.0.1", 7890, "https://api.openai.com/");
		client.sendMsg(LlmMessages.getInstance("写个小诗"), new BiConsumer<String, Boolean>() {
			@Override
			public void accept(String t, Boolean u) {
				if (u) {
					System.out.println(t);
				} else {
					System.out.print(t);
				}
			}
		},1,500);
		
	}
}
